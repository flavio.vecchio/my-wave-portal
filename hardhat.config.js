require("@nomiclabs/hardhat-waffle");

// This is a sample Hardhat task. To learn how to create your own go to
// https://hardhat.org/guides/create-task.html
task("accounts", "Prints the list of accounts", async (taskArgs, hre) => {
  const accounts = await hre.ethers.getSigners();

  for (const account of accounts) {
    console.log(account.address);
  }
});

// You need to export an object to set up your config
// Go to https://hardhat.org/config/ to learn more

/**
 * @type import('hardhat/config').HardhatUserConfig
 */
 require("@nomiclabs/hardhat-waffle");

 module.exports = {
   solidity: "0.8.4",
   networks: {
     rinkeby: {
       url: "https://eth-rinkeby.alchemyapi.io/v2/OYOKw3cwOz7WwGaeDYBl2iHXiogxJCFx",
       accounts: ["deb0f8ac626efb0ebf5f7b32ef89900c1a1d21c5d00a5f73cff91cd2a1baa5a8"],
     },
   },
 };
